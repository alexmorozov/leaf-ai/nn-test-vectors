This script generates test vectors for operations commonly used in neural
networks. It uses an arbitrary precision floating point library.

Generated test vectors are to be used in
https://github.com/autumnai/collenchyma-nn